import { ListAds, ListCategory, ListPost,ListConfig } from "../../wp-content/data";

function getMultipleRandom(arr: any, num: number) {
  const shuffled = [...arr].sort(() => 0.5 - Math.random());

  return shuffled.slice(0, num);
}

export const getAdsByHost = async (hostname: string) => {
  return ListAds[0];
};

export const getRecentPostsByHost = async (hostname: string) => {
  return getMultipleRandom(ListPost, 5);
};

export const getRecommendedPostsByHost = async (hostname: string) => {
  return getMultipleRandom(ListPost, 5);
};
export const getCategoriesByHost = async (hostname: string) => {
  return ListCategory;
};

export const getPostByHost = async (
  hostname: string,
  pathname: string,
  limit: number,
  offset: number
) => {
  const mHost = hostnameToTbl(hostname);
  const postTbl = mHost + "_posts";

  if (pathname !== "/" && pathname !== "") {
    const [post] = getMultipleRandom(ListPost, 1);
    return post;
  }

  const posts = getMultipleRandom(ListPost, 10);
  return posts;
};

export const getPostByCategoryByHost = async (
  hostname: string,
  pathname: string,
  limit: number,
  offset: number
) => {
  const category = ListCategory.find(
    (cate) => cate.slug == pathname.split("/").pop()
  );

  if (!category) {
    return [];
  }

  const posts = ListPost.find((post) => post.category == category.name);

  return posts;
};

const hostnameToTbl = (hostname: string) => {
  return hostname.replace(/\./g, "_");
};


export const getDetailSlug = async (path: any) => {
  return ListPost?.map((item) => { return {
    params: { layout: item?.slug || path },
    props: item,
  }});
};
export const getConfigByHost = async (hostname: string) => {
  return ListConfig[0] as IConfig;
};
export const getPostByCategories = async (pathname: string) => {
  return ListPost.filter((item) => item?.category === pathname);
};


export const getPathByCategories = async () => {
  return ListPost.map((item) => item?.category)
};