import {
  getAdsByHost,
  getCategoriesByHost,
  getConfigByHost,
  getDetailSlug,
  getPathByCategories,
  getPostByCategories,
  getPostByCategoryByHost,
  getPostByHost,
  getRecentPostsByHost,
  getRecommendedPostsByHost
} from './db'

const pageSize = 10

export const getPosts = async (hostname = '', pathname = '', page = '1') => {
  const start = (parseInt(page) - 1) * pageSize
  return getPostByHost(hostname, pathname, pageSize, start)
}
export const getDetail = async (path:any) => {
  return getDetailSlug(path);
};

export const getPostsByCategory = async (hostname = '', pathname = '', page = '1') => {
  const start = (parseInt(page) - 1) * pageSize
  return getPostByCategoryByHost(hostname, pathname, pageSize, start)
}

export const getCategories = async (hostname = '') => {
  return getCategoriesByHost(hostname)
}

export const getRecentPosts = async (hostname = '') => {
  return getRecentPostsByHost(hostname)
}

export const getRecommendedPosts = async (hostname = '') => {
  return getRecommendedPostsByHost(hostname)
}

export const getAds = async (hostname = '') => {
  const ads = await getAdsByHost(hostname)
  const unit = JSON.parse(ads.unit)
  return { ...ads, unit } as IAds
}
export const getConfigLayout = async (hostname = "") => {
  return getConfigByHost(hostname);
};

export const getAllPostOfCategory = async (pathname: string) => {
  return await getPostByCategories(pathname);
};

export const getAllPathOfCategory = async ()=>{
  return  getPathByCategories()
}
