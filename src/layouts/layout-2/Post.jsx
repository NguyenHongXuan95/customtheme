import { useState, useEffect } from 'preact/hooks'

const Post = () => {
    let [page, setPage] = useState(0)


    useEffect(()=>{
       if(typeof window.location.search.split('?page=')[1] !=='undefined'){
        setPage(parseInt(window.location.search.split('?page=')[1]) )
       }else{
        setPage(1)
       }
            
      
       
    },[page])
   
    const handleclick = ()=>{
             window.location.replace(`/?page=${typeof page === 'undefined' ? 2 : page+1}`)
      
       
    }
    const handleback = ()=>{
              window.location.replace(`/?page=${page-1}`)
       
    }
  return (
       
       <div className={'buttonPanigation'}>
            {page !== 1 ? (
            <button type={'back'} onClick={handleback}>
               Back
            </button>
            ): (<div />)}
            
            <span>{page}</span>
            
             <button type={"button"} onClick={handleclick}>
                Next
                </button>
          
      
    </div>
  )
}
export default Post