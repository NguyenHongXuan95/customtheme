import { atom } from "nanostores";

export const isOpen = atom(false);

export function ChangeTheme(check) {
  isOpen.set(check);
}
