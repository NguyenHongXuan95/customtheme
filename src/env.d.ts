/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />
/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

declare interface IPost {
  title: string;
  excerpt: string;
  content: string;
  slug: string;
  createdAt: number;
  categories: number[];
  category: { name: string };
  author: { name: string };
  id:number;
}

declare interface ICategory {
  name: string;
  slug: string;
}

declare interface IAds {
  pid: string;
  unit: { [key: string]: string };
  domain: string;
}

declare interface IConfig {
  layout: string;
  domain: string;
}